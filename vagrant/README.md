# Описание
Хостовая машина играет роль Ansible Control Node.
Запускаются 3 ВМ в Virtualbox. 
* Frontend
* Backend1
* Backend2

## Требования:
* Virtualbox -6.1
* Vagrant    -2.2.18
* Ansible    -2.9.6

## Подготовка к работе
### Создание виртуального окружения
cd vagrant &&
vagrant up

### Настройка IP адресов
1) Прописать в файле vagrant/hosts IP адреса своих ВМ.
2) Прописать в файле vagrant/zadanie/frontend/code/index.html IP адреса backend серверов.

### Запуск ansible playbook
ansible-playbook playbook.yml

## Разрушение виртуального окружения
vagrant destroy -f

